---?color=linear-gradient(to right, #e36877, #aaaaaa)
@title[The Selfish Team Player]

@snap[west text-20 text-bold text-white]
The Selfish Team Player
@snapend

@snap[south-west byline text-white text-06]
J. B. Rainsberger :: jbrains.ca :: @jbrains :: #atvie18
@snapend

---
@title[Slide Markdown]

### Each slide in this presentation is provided as a *template*.

<br><br>

@snap[south span-100 text-purple text-05]
Reuse the *markdown snippet* for any slide in this template within your own @css[text-gold text-bold](PITCHME.md) files.
@snapend

---
@title[Tip! Fullscreen]

![TIP](template/img/tip.png)
<br>
For the best viewing experience, press F for fullscreen.
@css[template-note](We recommend using the *SPACE* key to navigate between slides.)

---?include=template/md/split-screen/PITCHME.md

---?include=template/md/sidebar/PITCHME.md

---?include=template/md/list-content/PITCHME.md

---?include=template/md/boxed-text/PITCHME.md

---?include=template/md/image/PITCHME.md

---?include=template/md/sidebox/PITCHME.md

---?include=template/md/code-presenting/PITCHME.md

---?include=template/md/header-footer/PITCHME.md

---?include=template/md/quotation/PITCHME.md

---?include=template/md/announcement/PITCHME.md

---?include=template/md/about/PITCHME.md

---?include=template/md/wrap-up/PITCHME.md

---?image=https://images.jbrains.ca/jbrains-coaching.jpg
@snap[north-west docslink]
### **Don't stop @color[#e36877](here).**
@snapend

@snap[south docslink span-100 text-gold]
@jbrains :: jbrains.ca :: tdd.training
<br>
ask.jbrains.ca :: tell.jbrains.ca
<br>
blog.jbrains.ca :: blog.thecodewhisperer.com
@snapend
